mod twitch_video;

use anyhow::{anyhow, bail, Result};
use bytes::Bytes;
use futures::channel::{mpsc, oneshot};
use futures::prelude::*;
use indicatif::*;
use lazy_static::*;
use std::cell::UnsafeCell;

pub const CLIENT_ID: &'static str = "jzkbprff40iqj646a697cyrvl0zt2m6";
lazy_static! {
    pub static ref REQ_CLIENT: reqwest::Client = reqwest::Client::new();
    static ref TIME_PATTERN: regex::Regex = regex::Regex::new(
        r"^\s*(?:(?:(?:(\d+):)?(?:(\d+):))?(\d+)-)?(?:(?:(?:(\d+):)?(?:(\d+):))?(\d+))?\s*$"
    )
    .unwrap();
}
const MAX_FUTURE_NUM: usize = 500;

#[tokio::main]
async fn main() {
    let args = get_args(clap::App::new("Twitch Downloader"));
    let urls = get_urls(&args);
    let video_data: Vec<_> = stream::iter(urls)
        .then(|url| url_to_video_data(url))
        .map(|v| v.unwrap())
        .collect()
        .await;
    gstreamer::init().unwrap();
    download_video(video_data, &args).await.await.unwrap();
}

fn get_args<'a>(app: clap::App<'a, '_>) -> clap::ArgMatches<'a> {
    app.version(env!("CARGO_PKG_VERSION"))
        .author("SyuJyo <jayong93@gmail.com>")
        .about("Downloader for Twitch videos")
        .arg(
            clap::Arg::with_name("URL")
                .help("Video URLs to be downloaded. If neither an input file nor an url is provided, it reads urls from stdin.")
                .multiple(true)
                .index(1)
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("out_dir")
                .help("Where temporary videos and full videos will be saved in.")
                .short("o")
                .long("out-dir")
                .value_name("OUT_DIR")
                .default_value("."),
        )
        .arg(
            clap::Arg::with_name("input_file")
                .help("A file which has URLs to be downloaded")
                .short("i")
                .long("input")
                .value_name("INPUT_FILE"),
        )
        .arg(
            clap::Arg::with_name("duration")
                .help("Cut replay with specified duration")
                .long_help(
"Cut replay with specified duration.
The format is: [START_TIME-]END_TIME
Each time has same format: [[HOUR:]MIN:]SEC"
                )
                .short("d")
                .long("duration")
                .value_name("DURATION")
                .multiple(true)
                .number_of_values(1)
                .validator(|s| if TIME_PATTERN.is_match(&s) {Ok(())} else {Err("Wrong duration format. please read help message with '--help'".to_owned())})
        )
        .get_matches()
}

fn get_urls(args: &clap::ArgMatches<'_>) -> Vec<String> {
    if let Some(input_path) = args.value_of("input_file") {
        let input_file = std::fs::OpenOptions::new()
            .read(true)
            .open(input_path)
            .expect("Can't open the input file");
        read_urls_from(std::io::BufReader::new(input_file))
    } else {
        args.values_of("URL").map_or_else(
            || read_urls_from(std::io::stdin().lock()),
            |url_it| url_it.map(|s| s.to_string()).collect(),
        )
    }
}

async fn url_to_video_data(
    url: impl AsRef<str> + std::fmt::Debug + std::fmt::Display,
) -> Result<twitch_video::TwitchVideo> {
    let video_id = get_video_id(&url);
    match video_id {
        Some(VideoID::Clip(id)) => {
            let data_url = format!("https://api.twitch.tv/kraken/clips/{}", id);
            REQ_CLIENT
                .get(&data_url)
                .header("client-id", CLIENT_ID)
                .header("Accept", "application/vnd.twitchtv.v5+json")
                .send()
                .map_err(|e| anyhow!(e))
                .and_then(|res| {
                    res.json::<twitch_video::TwitchClipInfo>()
                        .map_err(|e| anyhow!(e))
                })
                .map_ok(|info| twitch_video::TwitchVideo::Clip(info))
                .await
        }
        Some(VideoID::Replay(id)) => {
            let data_url = format!("https://api.twitch.tv/kraken/videos/{}", id);
            REQ_CLIENT
                .get(&data_url)
                .header("client-id", CLIENT_ID)
                .send()
                .map_err(|e| anyhow!(e))
                // .inspect_ok(|res| eprintln!("{:?}", res))
                .and_then(|res| {
                    res.json::<twitch_video::TwitchReplayInfo>()
                        .map_err(|e| anyhow!(e))
                })
                .map_ok(|info| twitch_video::TwitchVideo::Replay(info))
                .await
        }
        None => bail!("Wrong URL: {}", url),
    }
}

fn read_urls_from(mut file: impl std::io::BufRead) -> Vec<String> {
    let mut buf = String::new();
    file.read_to_string(&mut buf)
        .expect("Can't read from stdin");
    buf.as_str()
        .split_whitespace()
        .map(|s| s.to_string())
        .collect()
}

#[derive(Debug)]
enum VideoID<'a> {
    Clip(&'a str),
    Replay(&'a str),
}

fn get_video_id<'a, T>(url: &'a T) -> Option<VideoID<'a>>
where
    T: AsRef<str> + std::fmt::Debug,
{
    use regex::*;
    lazy_static! {
        static ref REPLAY_URL_REGEX: Regex =
            Regex::new(r"https://www.twitch.tv(?:/[^/]*)?/video(?:s?)/(\d+).*").unwrap();
        static ref CLIP_URL_REGEX: Regex =
            Regex::new(r"https://(?:www.twitch.tv/[^/]+/clip|clips.twitch.tv)/([^?]+)(\?.*)?")
                .unwrap();
    }

    REPLAY_URL_REGEX.captures(url.as_ref()).map_or_else(
        || {
            CLIP_URL_REGEX
                .captures(url.as_ref())
                .and_then(|cap| cap.get(1).map(|m| VideoID::Clip(m.as_str())))
        },
        |cap| cap.get(1).map(|m| VideoID::Replay(m.as_str())),
    )
}

async fn download_video(
    video_list: Vec<twitch_video::TwitchVideo>,
    args: &clap::ArgMatches<'_>,
) -> oneshot::Receiver<()> {
    let out_dir = std::path::Path::new(args.value_of("out_dir").unwrap()).to_path_buf();
    std::fs::create_dir_all(&out_dir).expect("Can't create an output directory.");

    let multi_pb = MultiProgress::new();

    let times: Vec<_> = if let Some(time_it) = args.values_of("duration") {
        time_it
            .map(|time| {
                let caps = TIME_PATTERN.captures(time).unwrap();
                let times: Vec<usize> = caps
                    .iter()
                    .skip(1)
                    .map(|m| m.and_then(|mat| mat.as_str().parse().ok()).unwrap_or(0))
                    .collect();
                (
                    (times[0] * 3600 + times[1] * 60 + times[2]) as f64,
                    (times[3] * 3600 + times[4] * 60 + times[5]) as f64,
                )
            })
            .inspect(|(start_time, end_time)| {
                if *end_time != 0.0 {
                    assert!(
                        start_time <= end_time,
                        "Start time should be smaller than end time"
                    );
                }
            })
            .collect()
    } else {
        vec![]
    };

    let max_future_per_video = MAX_FUTURE_NUM / video_list.len();

    for (idx, video) in video_list.into_iter().enumerate() {
        let (start_time, end_time) = times
            .get(idx)
            .copied()
            .map(|v| if video.is_clip() { (0f64, 0f64) } else { v })
            .unwrap_or((0f64, 0f64));

        match (video.get_chunks().await, video.get_video_name()) {
            (Ok(chunks), out_name) => {
                let cumul_time = UnsafeCell::new(0f64);
                let content_length_list: Vec<_> = stream::iter(chunks)
                    .skip_while(|chunk| {
                        if start_time == 0.0 {
                            return future::ready(false);
                        }
                        let cumul_time = unsafe { &mut *cumul_time.get() };
                        let chunk_end_time = *cumul_time + chunk.length;
                        if chunk_end_time < start_time {
                            *cumul_time = chunk_end_time;
                            future::ready(true)
                        } else {
                            future::ready(false)
                        }
                    })
                    .take_while(|chunk| {
                        if end_time == 0.0 {
                            return future::ready(true);
                        }
                        let chunk_start_time = unsafe { &mut *cumul_time.get() };
                        if *chunk_start_time <= end_time {
                            *chunk_start_time += chunk.length;
                            future::ready(true)
                        } else {
                            future::ready(false)
                        }
                    })
                    .map(|chunk| {
                        REQ_CLIENT
                            .head(&chunk.url)
                            .header("client-id", CLIENT_ID)
                            .send()
                            .map_ok(|res| res.content_length().unwrap_or(0))
                            .map(|res| (res.unwrap_or(0), chunk))
                    })
                    .buffered(20)
                    .collect()
                    .await;

                let pb = ProgressBar::hidden();
                pb.set_style(
                    ProgressStyle::default_bar()
                        .template("{prefix:12}. [{elapsed_precise}] {wide_bar} {bytes}/{total_bytes} {bytes_per_sec}"),
                );
                pb.enable_steady_tick(1000);
                pb.set_length(content_length_list.iter().map(|(len, _)| len).sum());
                let pb = multi_pb.add(pb);

                let out_name = out_name.chars().fold(String::new(), |mut s, ch| {
                    match ch {
                        ':' | '?' | '*' => s.push('-'),
                        '<' | '>' | '"' | '|' => (),
                        c if std::path::is_separator(c) => s.push('-'),
                        c => s.push(c),
                    }
                    s
                });

                pb.set_prefix(&out_name);

                let mut out_path = out_dir.clone();
                out_path.push(out_name + ".mp4");

                tokio::spawn(async move {
                    let sender = data_send(out_path).await;
                    {
                        let pb = pb.clone();
                        stream::iter(content_length_list)
                            .map(|(_, chunk)| chunk)
                            .then(|chunk| {
                                REQ_CLIENT
                                    .get(&chunk.url)
                                    .header("client-id", CLIENT_ID)
                                    .send()
                            })
                            .map_err(|e| eprintln!("{}", anyhow!(e)))
                            .map_ok(|res| {
                                stream::unfold(Some(res), |res| {
                                    async {
                                        if let Some(mut res) = res {
                                            match res.chunk().await {
                                                Ok(Some(chunk)) => Some((Ok(chunk), Some(res))),
                                                Ok(None) => None,
                                                Err(e) => {
                                                    Some((Err(eprintln!("{}", anyhow!(e))), None))
                                                }
                                            }
                                        } else {
                                            None
                                        }
                                    }
                                })
                            })
                            .try_flatten()
                            .map(move |bytes| {
                                bytes
                                    .and_then(|b| {
                                        pb.inc(b.len() as u64);
                                        sender
                                            .unbounded_send(b)
                                            .map_err(|e| eprintln!("{}", anyhow!(e)))
                                    })
                                    .ok();
                                future::ready(())
                            })
                            .buffered(max_future_per_video)
                            .for_each(|_| future::ready(()))
                            .await;
                    }
                    pb.finish();
                });
            }
            (Err(e), _) => {
                eprintln!("Couldn't get a chunk list of. Error: {}", e);
            }
        }
    }

    let (sender, receiver) = oneshot::channel();

    std::thread::spawn(move || {
        multi_pb.join().unwrap();
        sender.send(()).ok();
    });

    receiver
}

async fn data_send(out_file: std::path::PathBuf) -> mpsc::UnboundedSender<Bytes> {
    use gst::prelude::*;
    use gstreamer as gst;
    use gstreamer_app as gst_app;

    let (sender, mut receiver) = mpsc::unbounded();

    tokio::spawn(async move {
        let concat = gst::ElementFactory::make("concat", Some("c")).unwrap();
        let fsink = gst::ElementFactory::make("filesink", None).unwrap();
        fsink
            .set_property("location", &out_file.to_str().unwrap())
            .unwrap();
        let appsrc = gst::ElementFactory::make("appsrc", None).unwrap();

        let pipeline = gst::Pipeline::new(None);
        pipeline.add_many(&[&concat, &fsink, &appsrc]).unwrap();
        gst::Element::link_many(&[&concat, &fsink]).unwrap();
        gst::Element::link_many(&[&appsrc, &concat]).unwrap();

        let appsrc = appsrc.dynamic_cast::<gst_app::AppSrc>().unwrap();

        pipeline.set_state(gst::State::Playing).unwrap();

        while let Some(b) = receiver.next().await {
            loop {
                if let Ok(_) = appsrc.push_buffer(gst::buffer::Buffer::from_slice(b)) {
                    break;
                } else {
                    unreachable!();
                }
            }
        }
        appsrc.end_of_stream().unwrap();

        let bus = pipeline.get_bus().unwrap();

        use gst::MessageView;
        for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
            match msg.view() {
                MessageView::Error(e) => {
                    eprintln!("{}", e.get_error());
                    break;
                }
                MessageView::Eos(_) => {
                    break;
                }
                _ => (),
            }
        }

        pipeline.set_state(gst::State::Null).unwrap();
    });

    sender
}
