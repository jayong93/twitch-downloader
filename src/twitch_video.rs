use anyhow::{anyhow, Result};
use futures::prelude::*;
use itertools::Itertools;
use serde_derive::*;

#[derive(Debug)]
pub enum TwitchVideo {
    Clip(TwitchClipInfo),
    Replay(TwitchReplayInfo),
}

#[derive(Deserialize, Debug)]
pub struct TwitchClipInfo {
    #[serde(flatten)]
    common_info: TwitchVideoInfo,
    slug: String,
    #[serde(rename = "broadcaster")]
    streamer: TwitchStreamer,
    #[serde(rename = "duration")]
    length: f64,
}

#[derive(Deserialize, Debug)]
pub struct TwitchReplayInfo {
    #[serde(flatten)]
    common_info: TwitchVideoInfo,
    #[serde(rename = "_id")]
    id: String,
    #[serde(rename = "channel")]
    streamer: TwitchStreamer,
    length: f64,
}

#[derive(Deserialize, Debug)]
struct TwitchVideoInfo {
    title: String,
    url: String,
    #[serde(rename = "created_at")]
    created_date: String,
}

#[derive(Deserialize, Debug)]
struct TwitchStreamer {
    name: String,
    display_name: String,
}

#[derive(Deserialize, Debug)]
struct TwitchAuthData {
    token: String,
    sig: String,
}

#[derive(Debug, Clone)]
pub struct VideoChunk {
    pub url: String,
    pub length: f64,
}

impl std::fmt::Display for TwitchVideo {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use TwitchVideo::*;
        match self {
            Clip(info) => write!(
                f,
                "[Clip] {} - {} (on {})({})",
                info.streamer.display_name,
                info.common_info.title,
                info.common_info.created_date,
                info.common_info.url
            ),
            Replay(info) => write!(
                f,
                "[Replay] {} - {} (on {})({})",
                info.streamer.display_name,
                info.common_info.title,
                info.common_info.created_date,
                info.common_info.url
            ),
        }
    }
}

#[derive(Deserialize, Debug)]
struct ClipQuality {
    quality: String,
    source: String,
}

#[derive(Deserialize, Debug)]
struct ClipStatus {
    status: String,
    quality_options: Vec<ClipQuality>,
}

impl TwitchVideo {
    pub async fn get_chunks(&self) -> Result<Vec<VideoChunk>> {
        use regex::*;
        match self {
            TwitchVideo::Clip(info) => {
                let req_url = format!("https://clips.twitch.tv/api/v2/clips/{}/status", info.slug);
                super::REQ_CLIENT
                    .get(&req_url)
                    .send()
                    .and_then(|res| res.json::<ClipStatus>())
                    .map(|status| {
                        status.map_err(|e| anyhow!(e)).and_then(|s| {
                            s.quality_options
                                .into_iter()
                                .nth(0)
                                .ok_or(anyhow!("No quality option in a clip"))
                                .map(|quality| {
                                    vec![VideoChunk {
                                        url: quality.source,
                                        length: info.length,
                                    }]
                                })
                        })
                    })
                    .await
            }
            TwitchVideo::Replay(info) => {
                let pattern = Regex::new(r"(https://.+/)[^/]+?\.m3u8$").unwrap();
                let video_id = info.id.trim_start_matches("v");
                let auth_data = get_auth_data(video_id).await?;
                let chunk_list_url = get_chunk_list_base_url(video_id, &auth_data).await?;
                let base_url = pattern
                    .captures_iter(&chunk_list_url)
                    .next()
                    .unwrap()
                    .get(1)
                    .unwrap()
                    .as_str();
                chunks_from_chunk_list(&chunk_list_url, base_url).await
            }
        }
    }

    pub fn get_video_name(&self) -> String {
        match self {
            TwitchVideo::Clip(info) => {
                info.streamer.display_name.clone()
                    + "-"
                    + &info.common_info.title
                    + "-"
                    + &info.common_info.created_date
            }
            TwitchVideo::Replay(info) => {
                info.streamer.display_name.clone()
                    + "-"
                    + &info.common_info.title
                    + "-"
                    + &info.common_info.created_date
            }
        }
    }

    pub fn is_replay(&self) -> bool {
        match self {
            Self::Clip(_) => false,
            Self::Replay(_) => true,
        }
    }

    pub fn is_clip(&self) -> bool {
        !self.is_replay()
    }
}

async fn get_auth_data(video_id: &str) -> Result<TwitchAuthData> {
    use percent_encoding::*;
    let auth_url = format!("https://api.twitch.tv/api/vods/{}/access_token?need_https=true&oauth_token&platform=web&player_backend=mediaplayer&player_type=popout", video_id);
    let mut auth_data = super::REQ_CLIENT
        .get(&auth_url)
        .header("client-id", super::CLIENT_ID)
        .send()
        .and_then(|res| res.json::<TwitchAuthData>())
        .map_err(|e| anyhow!(e))
        .await?;
    auth_data.token = utf8_percent_encode(&auth_data.token, NON_ALPHANUMERIC).collect();
    Ok(auth_data)
}

async fn get_chunk_list_base_url(video_id: &str, auth_data: &TwitchAuthData) -> Result<String> {
    let playlist_url = format!("https://usher.ttvnw.net/vod/{}.m3u8?allow_source=true&nauth={}&nauthsig={}&player_backend=mediaplayer&playlist_include_framerate=true", video_id, auth_data.token, auth_data.sig);
    super::REQ_CLIENT
        .get(&playlist_url)
        .header("client-id", super::CLIENT_ID)
        .send()
        .and_then(|res| res.text())
        .map_err(|e| anyhow!(e))
        .and_then(|body| {
            future::ready(
                body.lines()
                    .filter(|line| !line.starts_with("#"))
                    .filter(|line| line.contains("chunked"))
                    .nth(0)
                    .ok_or(anyhow!("No chunk list exists"))
                    .map(|s| (s.to_owned())),
            )
        })
        .await
}

async fn chunks_from_chunk_list(chunk_list_url: &str, base_url: &str) -> Result<Vec<VideoChunk>> {
    super::REQ_CLIENT
        .get(chunk_list_url)
        .header("client-id", super::CLIENT_ID)
        .send()
        .and_then(|res| res.text())
        .map_ok(|body| {
            body.lines()
                .filter(|line| line.starts_with("#EXTINF") || !line.starts_with("#"))
                .tuples::<(&str, &str)>()
                .map(|(time, file_name)| VideoChunk {
                    url: base_url.to_owned() + file_name,
                    length: time
                        .trim_start_matches("#EXTINF:")
                        .trim_end_matches(",")
                        .parse()
                        .unwrap(),
                })
                .collect()
        })
        .map_err(|e| anyhow!(e))
        .await
}
